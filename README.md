## Design challenge notes

### Primary metric
People signing up for webcast reminder

### Secondary goal
People signing up for newsletter

### Landing page for conversion
- Simple headline title followed by a quote from the presenter (A/B test possible)
- Sign up form right at the top with a clear header title (A/B test possible)
- Form CTA is the only primary one at the page (A/B test possible at the CTA label)
- Navigation was left to a minimal for increasing user focus (only link to GitLab at the top)
- Clear details on what users should expect from the webcast content (A/B test possible by adding illustrations to each item)
- Social proof with people quotes (A/B test possible by placing this section closer to the hero section)
- Mobile view with usable form in mobile devices

### Public URL
https://uberux.gitlab.io/webcast-landing-page-exercise/

### Project summary
- Mockup designed on [Sketch](https://gitlab.com/uberux/webcast-landing-page-exercise/blob/master/Sketch-File.zip)
- Static site implemented using Middleman
- CSS layout using flexbox
- Deployed automatically with GitLab CI + GitLab Pages